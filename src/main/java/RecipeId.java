package model;

import java.io.Serializable;
import java.util.Objects;

public class RecipeId implements Serializable {
    private int productId;
    private int rawMaterialId;

    public RecipeId() {
    }

    public RecipeId(int productId, int rawMaterialId) {
        this.productId = productId;
        this.rawMaterialId = rawMaterialId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getRawMaterialId() {
        return rawMaterialId;
    }

    public void setRawMaterialId(int rawMaterialId) {
        this.rawMaterialId = rawMaterialId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RecipeId)) return false;
        RecipeId recipeId = (RecipeId) o;
        return productId == recipeId.productId &&
                rawMaterialId == recipeId.rawMaterialId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, rawMaterialId);
    }
}
