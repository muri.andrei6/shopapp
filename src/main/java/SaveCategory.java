import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

public class SaveCategory implements ActionListener, ListSelectionListener, MouseListener {
    JPanel selectedProductPanel;
    JFrame frame;
    JPanel southProductPanel;

    String userName, password;

    String[] categoryHeaderLabel = {" Name "};
    JButton addButton, deleteButton, saveButton;
    JTable jTable;
    TableModel tableModel;
    DefaultTableModel model;

    JScrollPane tableScrollPane;
    ListSelectionModel rowSelectionModel;
    List<ProductCategory> productCategory;
    boolean successfully = false;

    SaveCategory(String userName, String password) {
        this.userName = userName;
        this.password = password;

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

        selectedProductPanel = new AddCustomPanels().addSimplePanel(Color.white, null, new GridLayout(), 5, 0);

        JPanel deletePanel = new AddCustomPanels().addSimplePanel(Color.white, null, new GridLayout(1, 2), 0, BoxLayout.X_AXIS);
        addButton = new AddCustomButtons().addSimpleButton(" Add category ", deletePanel, Color.yellow, Color.red, true, 100, 80);
        addButton.addActionListener(this);

        deleteButton = new AddCustomButtons().addSimpleButton(" Delete category ", deletePanel, Color.yellow, Color.red, false, 100, 80);
        deleteButton.addActionListener(this);

        JPanel saveButtonPanel = new AddCustomPanels().addSimplePanel(Color.white, null, new GridLayout(1, 1), 0, BoxLayout.X_AXIS);
        saveButton = new AddCustomButtons().addSimpleButton(" Save category ", saveButtonPanel, Color.black, Color.green, false, 100, 80);
        saveButton.addActionListener(this);

        southProductPanel = new AddCustomPanels().addSimplePanel(Color.white, null, null, 0, BoxLayout.Y_AXIS);
        southProductPanel.add(deletePanel);
        southProductPanel.add(saveButtonPanel);
        mainTable();
        initTable();

        frame = new JFrame();
        frame.setSize(dim.width, dim.height);
        frame.setBackground(Color.WHITE);
        frame.getContentPane().setLayout(new BorderLayout());
        frame.add(selectedProductPanel);
        frame.setTitle(" Category ");

        frame.setResizable(false);
        validate();
        frame.setVisible(true);
    }

    public void initTable() {
        jTable = new CreateTable().makeTable("", categoryHeaderLabel, true);
        model = (DefaultTableModel) jTable.getModel();
        tableModel = jTable.getModel();
        for (int i = 0; i < jTable.getColumnCount(); i++) {
            TableColumn col = jTable.getColumnModel().getColumn(i);
            jTable.getColumnModel().getColumn(i).setPreferredWidth(15);
            DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
            dtcr.setHorizontalAlignment(SwingConstants.CENTER);
            col.setCellRenderer(dtcr);
        }
        rowSelectionModel = jTable.getSelectionModel();
        rowSelectionModel.addListSelectionListener(this);
        jTable.addMouseListener(this);
    }

    public void mainTable() {
        selectedProductPanel.removeAll();
        selectedProductPanel.repaint();
    }

    public void validate() {
        selectedProductPanel.setLayout(new BorderLayout());
        tableScrollPane = new JScrollPane(jTable);
        tableScrollPane.setBackground(Color.white);
        tableScrollPane.addMouseListener(this);
        selectedProductPanel.add(tableScrollPane, BorderLayout.CENTER);
        selectedProductPanel.add(southProductPanel, BorderLayout.SOUTH);
        selectedProductPanel.validate();
        frame.validate();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(" Add category ")) {
            Object[] row = {""};
            model.addRow(row);
            enableButtons();
        }

        if (e.getActionCommand().equals(" Save category ")) {
            for (int i = 0; i < jTable.getRowCount(); i++) {
                productCategory = new ProductCategoryRepository(userName, password).searchByName((String) jTable.getValueAt(i, 0));
                if (productCategory.size() == 0) {
                    new ProductCategoryRepository(userName, password).saveProductCategory(new ProductCategory((String) jTable.getValueAt(i, 0)));
                } else {
                    new ShowMessage().showMessage("<html><p style=\"text-align:center;\"> Category already exist </p></html>", 2);
                }
            }
            jTable.clearSelection();
            model.getDataVector().removeAllElements();
            model.fireTableDataChanged();
            saveButton.setEnabled(false);
            deleteButton.setEnabled(false);
            successfully = true;
            frame.setVisible(false);
        }
        if (e.getActionCommand().equals(" Delete category ")) {
            if (jTable.getRowCount() >= 0) {
                model.removeRow(jTable.getSelectedRow());
                jTable.setModel(model);
            }
            enableButtons();
        }
    }

    public void enableButtons() {
        saveButton.setEnabled(jTable.getRowCount() > 0);
        deleteButton.setEnabled(jTable.getRowCount() > 0);
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        int selectedRows = jTable.getSelectedRow();
        if (selectedRows >= 0) deleteButton.setEnabled(true);
    }

    public boolean successfully() {
        return successfully;
    }
}
