import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

class Table_Products extends JFrame implements ActionListener {
    DecimalFormat df = new DecimalFormat("#.##");

    DefaultTableModel dtm;

    JTable jTable;

    String[] buttonName = {" Add ", " Delete "};
    String[] foodLabel = {" Raw Material ", " Price ", " Quantity "};

    JComboBox<Object> foodComboBox;
    JComboBox<Object> categoryComboBox;

    JTextField[] foodFields = new JTextField[3];

    JTextField recipesNameField, priceField;

    JPanel centerMasterPanel;
    JPanel foodLabelPanel, foodFieldPanel, foodPanel, foodButtonPanel, p1;

    static Object[] connectionOptionNames0 = {" Save ", " Cancel "};
    static Object[] connectionOptionNames1 = {" Modify ", " Cancel "};
    static Object[] connectionOptionNames2 = {" Cancel "};

    String userName, password;
    List<ProductCategory> categoryList = new ArrayList<>();
    List<Product> productsList = new ArrayList<>();
    List<RawMaterial> rawMaterialList = new ArrayList<>();
    List<String> rawMaterial = new ArrayList<>();
    List<String> catNames = new ArrayList<>();
    int productCategoryId;
    int productId;
    int rawMaterialId;

    List<RawMaterial> selectedRawMaterialList = new ArrayList<>();
    ProductCategoryRepository productCategoryRepository;
    AddCustomPanels addCustomPanels = new AddCustomPanels();
    ShowMessage showMessage = new ShowMessage();

    public Table_Products(String userName, String password) {
        this.userName = userName;
        this.password = password;
        productCategoryRepository = new ProductCategoryRepository(userName, password);
        initComponents();
    }

    public void initComponents() {
        productsList = new ProductRepository(userName, password).findAllProducts();

        JPanel categoryPanel = addCustomPanels.addBorderedPanel(Color.lightGray, BoxLayout.X_AXIS, Color.BLUE, " Category ", "Arial", Font.BOLD, 16, Color.BLUE, TitledBorder.CENTER, TitledBorder.TOP);
        categoryList = new ProductCategoryRepository(userName, password).findAllProductCategories();
        catNames = new ArrayList<>();
        for (ProductCategory c : categoryList) {
            catNames.add(c.getName());
        }
        categoryComboBox = new JComboBox<>(catNames.toArray());
        categoryComboBox.addActionListener(this);
        categoryComboBox.setActionCommand("categoryComboBox");
        categoryPanel.add(categoryComboBox);

        rawMaterialList = new RawMaterialRepository(userName, password).findAllRawMaterials();
        rawMaterial = new ArrayList<>();
        for (RawMaterial rm : rawMaterialList) {
            rawMaterial.add(rm.getName());
        }

        centerMasterPanel = new AddCustomPanels().addSimplePanel(Color.lightGray, new BorderLayout(), null, 0, 0);

        jTable = new CreateTable().makeTable("Recipes", foodLabel, false);
        dtm = (DefaultTableModel) jTable.getModel();/*Initializing the table model for myjtable*/
        JScrollPane jScrollPane1 = new JScrollPane(jTable);
        jScrollPane1.setViewportView(jTable);
        jScrollPane1.setIgnoreRepaint(false);

        foodLabelPanel = new AddCustomPanels().addSimplePanel(Color.lightGray, null, new GridLayout(foodLabel.length, 1), 0, 0);
        foodFieldPanel = new AddCustomPanels().addSimplePanel(Color.lightGray, null, new GridLayout(foodLabel.length, 1), 0, 0);

        for (int i = 0; i < foodLabel.length; i++) {
            new AddCustomLabels().addLabel(foodLabel[i], foodLabelPanel, "Arial", Font.BOLD, 18, new Color(102, 0, 153), Color.lightGray, SwingConstants.CENTER, null, JLabel.CENTER);
            if (i == 0) {
                foodComboBox = new JComboBox<>(rawMaterial.toArray());
                foodComboBox.addActionListener(this);
                foodComboBox.setActionCommand("foodComboBox");
                foodFieldPanel.add(foodComboBox);
            } else {
                foodFields[i] = new AddCustomFields().addField(6, foodFieldPanel, false, "0.00", true);
            }
            if (i == foodLabel.length - 1) {
                foodFields[i].setEditable(true);
                foodFields[i].setText("");
            }
        }

        foodPanel = new AddCustomPanels().addBorderedPanel(Color.lightGray, BoxLayout.X_AXIS, Color.BLUE, " Raw Material ", "Arial", Font.BOLD, 16, Color.BLUE, TitledBorder.CENTER, TitledBorder.TOP);
        foodPanel.add(foodLabelPanel);
        foodPanel.add(foodFieldPanel);

        foodButtonPanel = new AddCustomPanels().addSimplePanel(Color.lightGray, null, new GridLayout(1, buttonName.length), 0, 0);
        for (String s : buttonName) {
            JButton button = new AddCustomButtons().addButton(s, null, foodButtonPanel, new Color(102, 0, 153), new Color(64, 224, 208), 0, 1, true, 170, 30);
            button.addActionListener(this);
            button.setActionCommand(s);
        }

        centerMasterPanel.add(foodButtonPanel, BorderLayout.SOUTH);
        centerMasterPanel.add(foodPanel, BorderLayout.CENTER);

        JPanel recipesNamePanel = addCustomPanels.addBorderedPanel(Color.lightGray, BoxLayout.X_AXIS, Color.BLUE, " Product Name ", "Arial", Font.BOLD, 16, Color.BLUE, TitledBorder.CENTER, TitledBorder.TOP);
        recipesNameField = new AddCustomFields().addField(15, recipesNamePanel, true, "", true);
        JPanel pricePanel = addCustomPanels.addBorderedPanel(Color.lightGray, BoxLayout.X_AXIS, Color.BLUE, " Price ", "Arial", Font.BOLD, 16, Color.BLUE, TitledBorder.CENTER, TitledBorder.TOP);
        priceField = new AddCustomFields().addField(5, pricePanel, true, "", true);

        JPanel northPanel = addCustomPanels.addSimplePanel(Color.lightGray, null, new GridLayout(1, 3), 0, BoxLayout.X_AXIS);
        northPanel.add(categoryPanel);
        northPanel.add(recipesNamePanel);
        northPanel.add(pricePanel);
        p1 = addCustomPanels.addSimplePanel(Color.lightGray, new BorderLayout(), null, 0, 0);
        p1.add(northPanel, BorderLayout.NORTH);
        p1.add(centerMasterPanel, BorderLayout.CENTER);
        p1.add(jScrollPane1, BorderLayout.SOUTH);
    }

    public void activateAddDialog() {
        int nn = showMessage.showOption(null, "", connectionOptionNames0, p1);
        if (nn == 0) {
            ProductCategory productCategory = productCategoryRepository.findById(productCategoryId);
            Product newProduct = new Product(recipesNameField.getText(), Float.parseFloat(priceField.getText()), true, productCategory);
            new ProductRepository(userName, password).saveProduct(newProduct);

            for (RawMaterial material : selectedRawMaterialList) {
                new RecipeRepository(userName, password).saveRecipe(new Recipe(newProduct, material, material.getQuantity()));
            }
        } else showMessage.showMessage(" Operatiune abandonata !", 1);
    }

    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "categoryComboBox":
                for (ProductCategory category : categoryList) {
                    if (category.getName().equals(categoryComboBox.getSelectedItem())) {
                        productCategoryId = category.getProductCategoryId();
                    }
                }
                break;
            case "foodComboBox":
                for (RawMaterial rm : rawMaterialList) {
                    if (rm.getName().equals(foodComboBox.getSelectedItem())) {
                        foodFields[1].setText(String.valueOf(rm.getPrice()));
                        rawMaterialId = rm.getRawMaterialId();
                    }
                }
                break;
            case " Add ":
                float price = Float.parseFloat(foodFields[2].getText()) * Float.parseFloat(foodFields[1].getText());
                dtm.addRow(new Object[]{foodComboBox.getSelectedItem(), df.format(price), Float.parseFloat(foodFields[2].getText())});
                jTable.setModel(dtm);
                selectedRawMaterialList.add(new RawMaterial(rawMaterialId, (String) foodComboBox.getSelectedItem(), Float.parseFloat(foodFields[2].getText()), price));
                setPriceField();
                break;
            case " Delete ":
                if (jTable.getRowCount() >= 0) {
                    int xx = jTable.getSelectedRow();
                    dtm.removeRow(xx);
                    jTable.setModel(dtm);
                    setPriceField();
                } else showMessage.showMessage("Please select product!", 1);
                break;
        }
    }

    public void setPriceField() {
        float intermediatePrices = 0;
        for (int i = 0; i < jTable.getRowCount(); i++) {
            intermediatePrices += Float.parseFloat((String) jTable.getValueAt(i, 1));
        }
        priceField.setText(String.valueOf(intermediatePrices));
    }
}
