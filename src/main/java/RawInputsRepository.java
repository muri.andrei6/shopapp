import org.hibernate.query.Query;

import java.util.List;

public class RawInputsRepository extends SessionTransaction {
    String userName, password;

    RawInputsRepository(String userName, String password){
        this.userName = userName;
        this.password = password;
    }

    public void saveRawInputs(RawInputs rawInputs) {
        open();
        session.save(rawInputs);
        closeSession();
    }
    public List<RawInputs> findAllRawInputs() {
        open();
        String allRawInputs = ("FROM RawInputs");
        Query query = session.createQuery(allRawInputs);
        List<RawInputs> rawInputsList = ((org.hibernate.query.Query) query).list();
        closeSession();
        return rawInputsList;
    }
    public List<RawInputs> searchInputs(String name) {
        open();
        String allRawInputs = ("FROM RawInputs WHERE product = '" + name +"'");
        Query query = session.createQuery(allRawInputs);
        List<RawInputs> rawInputsList = ((org.hibernate.query.Query) query).list();
        closeSession();
        return rawInputsList;
    }
    public void open(){
        setUserAndPassword(userName, password);
        openSession();
    }
}
