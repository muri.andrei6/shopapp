import org.hibernate.query.Query;

import java.util.List;

public class MyRecipeRepository extends SessionTransaction{
        String userName, password;

        MyRecipeRepository(String userName, String password){
            this.userName = userName;
            this.password = password;
        }

        public List<MyRecipe> findAllRecipes() {
            open();
            String allRecipes = ("FROM MyRecipe");
            Query query = session.createQuery(allRecipes);
            List<MyRecipe> recipesList =  query.list();
            closeSession();
            return recipesList;
        }

        public void saveRecipe(MyRecipe recipe) {
            open();
            session.save(recipe);
            closeSession();
        }

    public List<MyRecipe> findByProductId(int id) {
        open();
        String searchByProductId = ("FROM MyRecipe WHERE productId = " + id);
        Query query = session.createQuery(searchByProductId);
        List<MyRecipe> recipeList = query.list();
        closeSession();
        return recipeList;
       }

        public void deleteRecipe(MyRecipe recipe) {
            open();
            session.delete(recipe);
            closeSession();
        }

        public List<Object[]> listWithProductAndMaterial(String sqlQuery){
            open();
            Query query = session.createQuery(sqlQuery);
            List<Object[]> recipeList = query.list();
            closeSession();
            return recipeList;
        }

        public void open(){
            setUserAndPassword(userName, password);
            openSession();
        }

}
