

import javax.persistence.*;

@Entity
@Table(name="recipe")
@IdClass(model.RecipeId.class)
public class Recipe {

    @Id
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="productId")
    private Product productId;


    @Id
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="rawMaterialId")
    private RawMaterial rawMaterialId;


    @Column
    private float quantity;

    public Recipe() {
    }

    public Recipe(Product product, RawMaterial rawMaterial, float quantity) {
        this.productId = product;
        this.rawMaterialId = rawMaterial;
        this.quantity = quantity;
    }

    public Product getProductId() {
        return productId;
    }

    public void setProductId(Product productId) {
        this.productId = productId;
    }

    public RawMaterial getRawMaterial() {
        return rawMaterialId;
    }

    public void setRawMaterial(RawMaterial rawMaterial) {
        this.rawMaterialId = rawMaterial;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "product=" + productId +
                ", rawMaterial=" + rawMaterialId +
                ", quantity=" + quantity +
                '}';
    }
}
