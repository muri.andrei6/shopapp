import javax.swing.*;
import java.awt.*;

public class AddCustomButtons {
    static JButton button;

    public JButton addButton(String text, Icon icon, JPanel p, Color foreground, Color background, int iconGap, int iconLeftSide, boolean state, int width, int height) {
        String[] splitStr = text.trim().split("\\s+");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<html><p style=\"text-align:center;\">");
        for (int i = 0; i < splitStr.length; i++) {
            stringBuilder.append(splitStr[i]);
            if (i < splitStr.length - 1) stringBuilder.append("<br>");
            else stringBuilder.append("</center></html>");
        }

        if (icon == null) {
            button = new JButton(String.valueOf(stringBuilder));
            button.setHorizontalAlignment(SwingConstants.CENTER);
        } else {
            if (p == null) {
                button = new JButton("", icon);
            } else {
                button = new JButton(String.valueOf(stringBuilder), icon);
                button.setIconTextGap(iconGap);

                switch (iconLeftSide) {
                    case 0:
                        button.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
                        button.setHorizontalAlignment(SwingConstants.LEFT);
                        button.setHorizontalTextPosition(SwingConstants.RIGHT);
                        break;
                    case 1:
                        button.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
                        button.setHorizontalAlignment(SwingConstants.RIGHT);
                        button.setHorizontalTextPosition(SwingConstants.LEFT);
                        break;
                }
            }
        }
        setButton(state, text, foreground, background, width, height);
        if (p != null) p.add(button);
        return button;
    }

    public JButton addSimpleButton(String text, JPanel p, Color foreground, Color background, boolean state, int width, int height) {
        button = new JButton(String.valueOf(text));
        button.setHorizontalAlignment(SwingConstants.CENTER);
        button.setFont(new Font("Arial", Font.BOLD, 24));
        setButton(state, text, foreground, background, width, height);
        p.add(button);
        return button;
    }

    public void setButton(boolean state, String text, Color foreground, Color background, int width,int height) {
        button.setPreferredSize(new Dimension(width, height));
        button.setEnabled(state);
        button.setActionCommand(text);
        button.setForeground(foreground);
        button.setBackground(background); // Purple
        button.setFocusPainted(false);
        button.setContentAreaFilled(false);
        button.setOpaque(true);
        button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    }
}
