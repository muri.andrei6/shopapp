import model.RecipeId;
import org.hibernate.query.Query;

import java.util.List;

public class RecipeRepository extends SessionTransaction{
    String userName, password;

    RecipeRepository(String userName, String password){
        this.userName = userName;
        this.password = password;
    }

    public List findAllRecipes() {
        open();
        String allRecipes = ("SELECT p.name, r.name, m.quantity FROM Recipe m INNER JOIN Product p ON p.productId = m.productId INNER JOIN RawMaterial r ON m.rawMaterialId = r.rawMaterialId");
        Query query = session.createQuery(allRecipes);
        List recipesList =  query.list();
        closeSession();
        return recipesList;
    }

    public void saveRecipe(Recipe recipe) {
        open();
        session.save(recipe);
        closeSession();
    }

        public List<Recipe> findByProductId(int id) {
        open();
        String allRecipes = ("FROM Recipe WHERE productId = "+ id);
            Query query = session.createQuery(allRecipes);
            List<Recipe> recipesList =  query.list();
            closeSession();
            return recipesList;
        }

    public void deleteRecipe(Recipe recipe) {
        open();
        session.delete(recipe);
        closeSession();
    }
    public void open(){
        setUserAndPassword(userName, password);
        openSession();
    }
}
