import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;

class CreateTable {
    public JTable makeTable(String name, String[] columnName, boolean state) {
        JTable jTable1 = new JTable();
        jTable1.setName(name);
        if (columnName != null) {
            jTable1.setModel(new DefaultTableModel(new Object[][]{}, columnName));
        } else {
            jTable1.setModel(new DefaultTableModel(new Object[][]{}, new String[]{}));
        }
        jTable1.setColumnSelectionAllowed(state);
        jTable1.setRowSelectionAllowed(true);
        jTable1.setRowHeight(40);
        jTable1.setShowGrid(false);

        jTable1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        if (!state) jTable1.setDefaultEditor(Object.class, null);
        return jTable1;
    }
}
