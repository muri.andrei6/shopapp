import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "productId")
    private int productId;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private float price;

    @Column(name = "state")
    private boolean state;

    @OneToMany(cascade = CascadeType.ALL, mappedBy="productId")
    private List<Recipe> recipes=new ArrayList<>();

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "productCategoryId")
    private ProductCategory productCategory;

    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "products")
    private final List<Order> orders = new ArrayList<>();

    public Product() {
    }

    public Product(  String name, float price, boolean state, ProductCategory productCategory) {
        this.name = name;
        this.price = price;
        this.state = state;
        this.productCategory = productCategory;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public List<Recipe> getRecipes() {
        return recipes;
    }

    public void setRecipes(List<Recipe> recipes) {
        this.recipes = recipes;
    }

   public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

   /*

    public int getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(int productCategory) {
        this.productCategory = productCategory;
    }
 */
    public List<Order> getOrders() {
        return orders;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", state=" + state +
                ", recipes=" + recipes +
                ", productCategory=" + productCategory +
                ", orders=" + orders +
                '}';
    }
}
