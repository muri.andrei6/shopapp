
import javax.swing.*;
import java.awt.*;

public class LookAndFeel {

    public LookAndFeel() {
        UIDefaults uiDefaults = UIManager.getDefaults();

        uiDefaults.put("activeCaption", Color.blue); // color for bar window
        uiDefaults.put("activeCaptionText", Color.yellow); // color for text window
        uiDefaults.put("activeCaptionBackground", Color.white);

        uiDefaults.put("Button.background", new Color(128, 0, 128));
        uiDefaults.put("Button.foreground", Color.white);
        uiDefaults.put("Button.focus", new Color(0, 0, 0, 0));
        uiDefaults.put("Button.border", BorderFactory.createEmptyBorder(10, 10, 10, 10));
        uiDefaults.put("Button.font", new Font("Arial", Font.BOLD, 16));
        uiDefaults.put("Button.select", Color.white);

        uiDefaults.put("ComboBox.font", new Font("Arial", Font.BOLD, 16));
        uiDefaults.put("ComboBox.background", Color.YELLOW);

        uiDefaults.put("Menu.opaque", true);
        uiDefaults.put("Menu.background", Color.CYAN);
        uiDefaults.put("Menu.foreground", Color.BLACK);
        uiDefaults.put("Menu.font", new Font("Arial", Font.BOLD, 20));
        uiDefaults.put("Menu.selectionBackground", Color.BLACK);
        uiDefaults.put("Menu.selectionForeground", Color.CYAN);

        uiDefaults.put("MenuItem.opaque", true);
        uiDefaults.put("MenuItem.background", Color.blue);
        uiDefaults.put("MenuItem.foreground", Color.yellow);
        uiDefaults.put("MenuItem.font", new Font("Arial", Font.BOLD, 18));
        uiDefaults.put("MenuItem.selectionBackground", Color.yellow);
        uiDefaults.put("MenuItem.selectionForeground", Color.blue);

        uiDefaults.put("OptionPane.background", Color.lightGray);
        uiDefaults.put("OptionPane.messageFont", new Font("Courier", Font.PLAIN, 16));//marimea textului de la mesaje
        uiDefaults.put("OptionPane.buttonFont", new Font("Arial", Font.BOLD, 16)); // marimea fontului JOptionPane
        uiDefaults.put("OptionPane.sameSizeButtons", true);

        uiDefaults.put("RadioButton.font", new Font("Arial", Font.BOLD, 16));
        uiDefaults.put("RadioButton.background", new Color(64, 224, 208));

        uiDefaults.put("RadioButtonGroup.background", new Color(64, 224, 208));

        uiDefaults.put("Panel.background", Color.lightGray);

        uiDefaults.put("Table.font", new Font("Courier", Font.PLAIN, 16));
        uiDefaults.put("Table.foreground", Color.black);
        uiDefaults.put("Table.background", Color.white);
        uiDefaults.put("Table.selectionBackground", Color.blue);
        uiDefaults.put("Table.selectionForeground", Color.white);

        uiDefaults.put("TableHeader.background", Color.black);
        uiDefaults.put("TableHeader.font", new Font("Verdana", Font.ITALIC, 20));
        uiDefaults.put("TableHeader.foreground", Color.white);

        uiDefaults.put("TextField.background", Color.white);
        uiDefaults.put("TextField.font", new Font("Arial", Font.BOLD, 18));
        uiDefaults.put("TextField.foreground", Color.red);

        uiDefaults.put("Tree.font", new Font("Arial", Font.BOLD, 16));
        uiDefaults.put("Tree.foreground ", Color.blue);
        uiDefaults.put("Tree.closedIcon", new TreeIcon());
        uiDefaults.put("Tree.openIcon", new TreeIcon());
        uiDefaults.put("Tree.leafIcon", new TreeIcon());
        uiDefaults.put("Tree.rootVisible", false);
        uiDefaults.put("Tree.selectionForeground", Color.white);
        uiDefaults.put("Tree.selectionBackground", Color.blue);

        JFrame.setDefaultLookAndFeelDecorated(true);
        JDialog.setDefaultLookAndFeelDecorated(true);
    }

    static class TreeIcon implements Icon {

        private final int SIZE = 0;

        public TreeIcon() {
        }

        public int getIconWidth() {
            return SIZE;
        }

        public int getIconHeight() {
            return SIZE;
        }

        public void paintIcon(Component c, Graphics g, int x, int y) {
        }
    }
}