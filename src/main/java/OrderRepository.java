import org.hibernate.query.Query;

import java.util.List;

public class OrderRepository extends SessionTransaction {

    String userName, password;

    OrderRepository(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public void saveOrder(Order order) {
        open();
        session.save(order);
        closeSession();
    }

    public void deleteOrder(Order order) {
        open();
        session.delete(order);
        closeSession();
    }

    public boolean findByOrderId(int id) {
        open();
        Order order = session.find(Order.class, id);
        closeSession();
        return order != null;
    }

    public List findAllOrders() {
        open();
        String allOrders = ("FROM Order");
        Query query = session.createQuery(allOrders);
        List orderList = query.list();
        closeSession();
        return orderList;
    }

    public int getMaxId() {
        open();
        Query query = session.createQuery("SELECT MAX(orderId) FROM Order");
        List list = query.list();
        closeSession();
        return (int) list.get(0);
    }

    public void open() {
        setUserAndPassword(userName, password);
        openSession();
    }
}
