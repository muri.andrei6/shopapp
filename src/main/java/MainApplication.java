import javax.swing.*;
import java.awt.*;

public class MainApplication {
    private final static Object[] connectionOptionNames = {" User ", " Admin ", " Barcode Reader"};
    ShowMessage showMessage = new ShowMessage();

    public MainApplication() {
        new LookAndFeel();
        int selectedOption = showMessage.showOption("", "", connectionOptionNames, loginComponent());
        if (selectedOption == 0) {
            new Shop_1("user", "12345678");
           }
        else if (selectedOption == 1) {
           new AdminLogin();
        }
        else if (selectedOption == 2) {
           new BarcodeScanner("user", "12345678");
        }
        else System.exit(0);
    }

    public JPanel loginComponent() {
        JPanel loginPanel = new AddCustomPanels().addSimplePanel(Color.lightGray, new BorderLayout(), null, 10, 0);
        new AddCustomLabels().addLabel(" eSHOP ", loginPanel, "Arial", Font.ITALIC, 44, Color.red, Color.lightGray, SwingConstants.CENTER, BorderLayout.NORTH, JLabel.CENTER);
        new AddCustomLabels().addLabel("\u00a9 2020", loginPanel, "Courier", Font.ITALIC, 15, Color.blue, Color.lightGray, SwingConstants.CENTER, BorderLayout.SOUTH, JLabel.CENTER);
        return loginPanel;
    }

    public static void main(String[] args){
        new MainApplication();
    }

}
