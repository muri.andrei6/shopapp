import javax.swing.*;
import java.awt.*;

public class LoginComponent {
    static JTextField userNameField;
    static JPasswordField passwordField;

    public static JPanel userLoginPanel() {

        JPanel userPanel = new AddCustomPanels().addSimplePanel(Color.lightGray, null, new GridLayout(4, 1), 5, 0);

        new AddCustomLabels().addLabel(" User Name ", userPanel, "Arial", Font.BOLD, 20, Color.black, Color.lightGray, SwingConstants.CENTER, null, JLabel.CENTER);
        userNameField = new AddCustomFields().addField(16, userPanel, true, "", true);
        new AddCustomLabels().addLabel(" Password ", userPanel, "Arial", Font.BOLD, 20, Color.black, Color.lightGray, SwingConstants.CENTER, null, JLabel.CENTER);
        passwordField = new AddCustomFields().addPasswordField(userPanel);
        JPanel loginPanel = new AddCustomPanels().addSimplePanel(Color.lightGray, new BorderLayout(), null, 10, 0);
        loginPanel.add(userPanel, BorderLayout.CENTER);
        new AddCustomLabels().addLabel("\u00a9 2020", loginPanel, "Courier", Font.ITALIC, 15, Color.blue, Color.lightGray, SwingConstants.CENTER, BorderLayout.SOUTH, JLabel.CENTER);

        return loginPanel;
    }

    public static String getUser() {
        return userNameField.getText();
    }

    public static String getPass() {
        return String.valueOf(passwordField.getPassword());
    }

    public static void setText(String text) {
        userNameField.setText(text);
        passwordField.setText(text);
    }
}
