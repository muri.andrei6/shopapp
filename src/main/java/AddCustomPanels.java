import javax.swing.*;
import java.awt.*;

public class AddCustomPanels {

    public JPanel addSimplePanel(Color background, BorderLayout borderLayout, GridLayout gridLayout, int vGap, int boxLayout) {
        JPanel panel = new JPanel();
        panel.setBackground(background);
        if (borderLayout != null) {
            panel.setLayout(borderLayout);
            borderLayout.setVgap(vGap);
        } else if (gridLayout != null) {
            panel.setLayout(gridLayout);
            gridLayout.setVgap(vGap);
        } else panel.setLayout(new BoxLayout(panel, boxLayout));
        return panel;
    }

    public JPanel addBorderedPanel(Color background, int layout, Color colorBorder, String namePanel, String fontName, int font, int size, Color colorFont, int alignment, int position) {
        JPanel panel = new JPanel();
        panel.setBackground(background);
        panel.setLayout(new BoxLayout(panel, layout));
        panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(colorBorder), namePanel, alignment, position, new Font(fontName, font, size), colorFont));
        return panel;
    }

    public JPanel addNoLayoutBorderedPanel(Color background, Color colorBorder, String namePanel, String fontName, int font, int size, Color colorFont, int alignment, int position) {
        JPanel panel = new JPanel();
        panel.setBackground(background);
        panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(colorBorder), namePanel, alignment, position, new Font(fontName, font, size), colorFont));
        return panel;
    }
}

