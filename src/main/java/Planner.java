import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.time.*;
import java.util.*;

class Planner implements ActionListener {
    static LocalDate date, localDate;
    String dateString;

    static JRadioButton[] hourButton = new JRadioButton[24];

    JPanel hourPanel;

    ButtonGroup radioButtonGroup = new ButtonGroup();
    String selectedHour;

    public Planner() {

        localDate = LocalDate.now();

        hourPanel = new AddCustomPanels().addSimplePanel(new Color(64, 224, 208), null, new GridLayout(8, 3), 0, 0);
        for (int i = 0; i < 24; i++) {
            if(i < 10){hourButton[i] = addRadioButton("0" + i + ":00", hourPanel);}
            else hourButton[i] = addRadioButton(i + ":00", hourPanel);
        }
    }

    public JRadioButton addRadioButton(String name, JPanel panel) {
        JRadioButton radioButton = new JRadioButton(name);
        radioButton.setActionCommand(name);
        radioButton.addActionListener(this);
        radioButtonGroup.add(radioButton);
        panel.add(radioButton);
        return radioButton;
    }

    public JPanel getMasterPanel() {
        return hourPanel;
    }

    public static void updateAppointmentsPanel(LocalDate date) {
        GregorianCalendar today = new GregorianCalendar();
        for (int i = 0; i < 24; i++) {
            if (convertStringToTime(hourButton[i].getActionCommand() + ":00").isAfter(LocalTime.of(today.get(Calendar.HOUR_OF_DAY), 0, 0)) && (date.compareTo(localDate) == 0)) {
                setButton(hourButton[i], false);
            } else if (date.compareTo(localDate) < 0) setButton(hourButton[i], true);
        }
    }

    public String getSelectedHour() {
        return selectedHour;
    }

    public void actionPerformed(ActionEvent e) {
        for (int i = 0; i < 24; i++) {
            if (hourButton[i].isSelected()) {
                selectedHour = hourButton[i].getActionCommand();
            }
        }
    }

    public static void setButton(JRadioButton button, boolean state) {
        button.setEnabled(state);
    }

    public static LocalTime convertStringToTime(String tempString) {
        String[] convert = tempString.split(":");
        return LocalTime.of(Integer.parseInt(convert[0]), 0, 0);
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    static void setCurrentData(LocalDate date) {
        updateAppointmentsPanel(Planner.date = date);
    }
}

