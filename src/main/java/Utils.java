import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import java.util.Properties;

public class Utils {
    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory(String userName, String password) {
        if (sessionFactory == null) {
            try {
                Configuration configuration = new Configuration();
                Properties settings = new Properties();
                  settings.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
                //   settings.put(Environment.URL, "jdbc:mysql://192.168.1.200:3306/shopapp?autoReconnect=true&useSSL=false");
                  settings.put(Environment.URL, "jdbc:mysql://localhost:3306/shopapp?autoReconnect=true&useSSL=false");
                settings.put(Environment.USER, userName);
                settings.put(Environment.PASS, password);
                settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5InnoDBDialect");
                settings.put(Environment.SHOW_SQL, "true");
                settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
                settings.put(Environment.HBM2DDL_AUTO, "update");
                configuration.setProperties(settings);
                configuration.addAnnotatedClass(Product.class);
                configuration.addAnnotatedClass(Order.class);
                configuration.addAnnotatedClass(ProductCategory.class);
                configuration.addAnnotatedClass(RawMaterial.class);
                configuration.addAnnotatedClass(RawInputs.class);
                configuration.addAnnotatedClass(Recipe.class);
              //  configuration.addAnnotatedClass(MyRecipe.class);
                ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
                sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            } catch (Exception e) {
                //  e.printStackTrace();
            }
        }
        return sessionFactory;
    }
}



