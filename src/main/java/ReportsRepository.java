import org.hibernate.query.Query;

import java.util.List;

public class ReportsRepository extends SessionTransaction {

    String userName, password;

    ReportsRepository(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public List<Order> getCount(String selectedDate) {
        open();
        //  Long count = (Long) session.createQuery(
        //          "SELECT COUNT(orderId) FROM Order WHERE DATE(dateAndTime) = '" + selectedDate + "'").uniqueResult();
        Query query = session.createQuery(selectedDate);
        List<Order> orderList = query.list();
        closeSession();
        return orderList;
    }

    public Double getSum(String sqlQuery) {
        open();
        Double count = (Double) session.createQuery(sqlQuery).uniqueResult();
        closeSession();
        return count;
    }

    public List getOrdersAndProduct() {
        open();
        Query query = session.createQuery("FROM Order o, orders_products op WHERE o.orderId=op.orderId");
        //  Query query = session.createQuery("SELECT Order.dateAndTime, Order.price, orders_products.productId, orders_products.quantity FROM Order INNER JOIN orders_products USING(orderId)");
        List orderList = query.list();
        closeSession();
        return orderList;

    }


    public void open() {
        setUserAndPassword(userName, password);
        openSession();
    }
}
