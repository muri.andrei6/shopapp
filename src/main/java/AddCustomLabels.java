import javax.swing.*;
import java.awt.*;

class AddCustomLabels {

    public void addLabel(String name, JPanel p, String nameFont, int font, int size, Color color, Color color1, int alignment, String layout, int horAlignment) {
        JLabel label = new JLabel(name, alignment);
        label.setHorizontalAlignment(horAlignment);
        label.setFont(new Font(nameFont, font, size));
        label.setOpaque(true);
        label.setForeground(color);
        label.setBackground(color1);
        label.setAlignmentY(Component.CENTER_ALIGNMENT);
        p.add(label, layout);
    }

    public static JLabel addLabel(String name, JPanel p, String nameFont, int font, int size, Color color, Color color1, int alignment) {
        JLabel label = new JLabel(name, alignment);
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setFont(new Font(nameFont, font, size));
        label.setOpaque(true);
        label.setForeground(color);
        label.setBackground(color1);
        p.add(label);
        return label;
    }
}

