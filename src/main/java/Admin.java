import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.*;
import java.awt.event.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class Admin extends FormatDateTime implements ActionListener, MouseListener {
    JPanel selectedProductPanel, westPanel;
    JFrame frame;

    JTree tree;
    Map<String, String[]> treeNodes = new HashMap<>();

    String userName, password, tableName;

    JScrollPane tableScrollPane;
    JTable jTable;
    TableModel tableModel;
    ListSelectionModel rowSelectionModel;
    DefaultTableModel model;

    JMenuItem menuItem;
    JPopupMenu popup;
    JMenu record;

    boolean success = false;

    String[] notificationOption = {"Yes", "No"};

    String[] productHeaderLabel = {"Product", "Price", "Availability"};
    String[] recipeHeaderLabel = {"Product", "Raw Material", "Quantity"};
    String[] orderHeaderLabel = {"Date and Time", "Price", "Products"};
    String[] inputsHeaderLabel = {"Date", "Product", "Quantity", "Price"};
    String[] rawMaterialsHeaderLabel = {"Product", "Quantity", "Price"};
    String[] categoryHeaderLabel = {"Name"};

    List<RawInputs> rawInputsList;
    List<RawMaterial> rawMaterialsList;
    List<RawMaterial> selectedRawMaterialModifyList;
    List<ProductCategory> productCategoriesList;
    List<Product> productsList;
    List<Product> selectedProductModifyList;
    List<Order> ordersList;
    Table_Products table_products;

    String selectedDate;
    String stock;
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    LocalDateTime dateTime;

    Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
    JTextField modifyPrice;

    static Object[] connectionOptionNames = {" Modify ", " Cancel "};

    AddCustomPanels addCustomPanels = new AddCustomPanels();
    ShowMessage showMessage = new ShowMessage();

    AppCalendar calendarApplication = new AppCalendar();

    public Admin(String userName, String password) {
        this.userName = userName;
        this.password = password;

        table_products = new Table_Products(userName, password);

        treeNodes.put("Inputs", new String[]{"Inputs", "Raw materials"});
        treeNodes.put("Reports", new String[]{"Orders", "Monthly", "Daily", "Hourly", "Monthly income/expenses", "Minimal stock", "Critical stock"});
        treeNodes.put("Products Category", new String[]{"Category"});
        treeNodes.put("Products", new String[]{"Products", "Recipes"});

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Root");

        for (Map.Entry<String, String[]> entry : treeNodes.entrySet()) {
            DefaultMutableTreeNode inputsNode = new DefaultMutableTreeNode(entry.getKey());
            root.add(inputsNode);
            for (int i = 0; i < entry.getValue().length; i++) {
                inputsNode.add(new DefaultMutableTreeNode(entry.getValue()[i]));
            }
        }

        tree = new JTree(root);
        tree.setShowsRootHandles(true);
        tree.setRootVisible(false);
        DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer) tree.getCellRenderer();
        renderer.setTextSelectionColor(Color.white);
        renderer.setBackgroundSelectionColor(Color.blue);
        renderer.setBorderSelectionColor(Color.black);

        tree.getSelectionModel().addTreeSelectionListener(e -> {
            try {
                if (Objects.requireNonNull(tree.getSelectionPath()).getPathCount() == 3) {
                    tableName = tree.getLastSelectedPathComponent().toString();
                    initTable();
                }
            } catch (NullPointerException ignored) {
            }
        });
        JScrollPane treeScrollPane = new JScrollPane(tree);
        treeScrollPane.setBorder(BorderFactory.createEmptyBorder());
        westPanel = addCustomPanels.addSimplePanel(Color.white, null, new GridLayout(), 5, 0);
        westPanel.add(treeScrollPane);

        westPanel.setPreferredSize(new Dimension(250, dim.height - 10));
        westPanel.setMaximumSize(new Dimension(250, dim.height));

        selectedProductPanel = addCustomPanels.addSimplePanel(Color.white, null, new GridLayout(), 5, 0);

        frame = new JFrame();
        frame.setSize(dim.width, dim.height);
        frame.setBackground(Color.WHITE);
        frame.getContentPane().setLayout(new BorderLayout());
        frame.add(westPanel, BorderLayout.WEST);
        frame.add(selectedProductPanel, BorderLayout.CENTER);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle(" Admin ");

        frame.setResizable(false);
        frame.setVisible(true);
    }

    public void initTable() {
        switch (tableName) {
            case "Inputs":
            case "Raw materials":
            case "Category":
            case "Products":
            case "Recipes":
            case "Orders":
                createTable();
                validate();
                break;
            case "Monthly":
            case "Daily":
                calendarApplication.setView(false);
                selectedDate = calendarApplication.activateViewCalendar();
                createTable();
                break;
            case "Hourly":
                try {
                    calendarApplication.setView(true);
                    dateTime = LocalDateTime.parse(calendarApplication.activateViewCalendar());
                    createTable();
                } catch (NullPointerException ignored) {
                }
                break;
            case "Minimal stock":
            case "Critical stock":
                showNotification();
                createTable();
                break;
            case "Monthly income/expenses":
                calendarApplication.setView(false);
                selectedDate =calendarApplication.activateViewCalendar();
                createIncomeCosts();
                break;
        }
    }

    public void createIncomeCosts() {
        String[] yearAndMonth = selectedDate.split("-");
        double orderIncome = Math.round(new ReportsRepository(userName, password).getSum("SELECT SUM(price) FROM Order WHERE YEAR(dateAndTime) = '" + yearAndMonth[0] + "' AND MONTH(dateAndTime) = '" + yearAndMonth[1] + "'"));
        double inputsPrice = Math.round(new ReportsRepository(userName, password).getSum("SELECT SUM(quantity*price) FROM RawInputs WHERE YEAR(date) = '" + yearAndMonth[0] + "' AND MONTH(date) = '" + yearAndMonth[1] + "'"));
        double ratio = Math.round(Math.max(orderIncome, inputsPrice) / 10);
        JPanel panel = addCustomPanels.addNoLayoutBorderedPanel(Color.lightGray, Color.BLUE, " Expenses vs. Income", "Arial", Font.BOLD, 24, Color.BLUE, TitledBorder.CENTER, TitledBorder.TOP);
        panel.add(drawPanel(inputsPrice, ratio, " Expenses "));
        panel.add(drawPanel(orderIncome, ratio, " Income "));
        panel.setAlignmentX(Component.CENTER_ALIGNMENT);
        selectedProductPanel.removeAll();
        selectedProductPanel.repaint();
        selectedProductPanel.add(panel);
        selectedProductPanel.validate();
        frame.validate();
    }

    public JPanel drawPanel(double firstDouble, double ratio, String name) {
        JLabel[] level = new JLabel[10];
        JPanel masterPanel = addCustomPanels.addBorderedPanel(Color.lightGray, BoxLayout.Y_AXIS, Color.RED, name, "Arial", Font.BOLD, 16, Color.RED, TitledBorder.CENTER, TitledBorder.TOP);
        masterPanel.setLayout(new BorderLayout());
        JPanel labelPanel = addCustomPanels.addSimplePanel(Color.white, null, new GridLayout(1, 1), 0, 0);
        AddCustomLabels.addLabel(String.valueOf(firstDouble), labelPanel, "Arial", Font.BOLD, 20, Color.black, Color.white, SwingConstants.CENTER);
        JPanel levelPanel = addCustomPanels.addSimplePanel(Color.white, null, new GridLayout(level.length, 1), 0, 0);
        for (int i = 0; i < level.length; i++) {
            level[i] = AddCustomLabels.addLabel("   ", levelPanel, "Arial", Font.BOLD, 20, Color.black, Color.white, SwingConstants.CENTER);
            if (i >= level.length - Math.round(firstDouble / ratio)) {
                level[i].setBackground(Color.green);
            }
        }
        masterPanel.add(labelPanel, BorderLayout.NORTH);
        masterPanel.add(levelPanel, BorderLayout.CENTER);
        masterPanel.setPreferredSize(new Dimension(120, dim.height - 200));
        masterPanel.setMaximumSize(new Dimension(120, dim.height - 150));
        return masterPanel;
    }

    public void showNotification() {
        int n = showMessage.showOption("Do you want to turn on notification?", "Notification", notificationOption, null);
        if (n == 0) {
            frame.setVisible(false);
            //TODO apelam metoda de notificare pentru stocuri in functie de stringul stock
        } else {
            if (tableName.equals("Minimal stock")) {
                stock = "FROM RawMaterial WHERE quantity < 10 AND quantity > 5";
            } else stock = "FROM RawMaterial WHERE quantity < 5";
        }
    }

    public void generalSearch() {
        String searchString = showMessage.showInputDialog();
        if (searchString.equals("")) {
            showMessage.showMessage("Complete search criteria", 1);
        } else {
            switch (tableName) {
                case "Inputs":
                case "Raw materials":
                case "Category":
                case "Products":
                case "Recipes":
                    searchName(searchString);
                    validate();
                    break;
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(" Refresh ")) {
            initTable();
        }

        if (e.getActionCommand().equals(" General search ")) {
            generalSearch();
        }

        if (e.getActionCommand().equals(" Add record ")) {
            switch (tableName) {
                case "Inputs":
                    success = new SaveRawMaterial(userName, password).successfully();
                    break;
                case "Category":
                    success = new SaveCategory(userName, password).successfully();
                    break;
                case "Products":
                    new Table_Products(userName, password).activateAddDialog();
                    break;
            }
            if (success) {
                createTable();
                validate();
            }
        }
        if (e.getActionCommand().equals(" Delete record ")) {
            switch (tableName) {
                case "Category":
                    success = new SaveCategory(userName, password).successfully();
                    break;
                case "Products":
                    new Table_Products(userName, password).activateAddDialog();
                    break;
            }
        }
        if (e.getActionCommand().equals(" Modify record ")) {
            switch (tableName) {
                case "Raw materials":
                    activateModifyRawMaterialDialog();
                    break;
                case "Products":
                    activateModifyProductDialog();
                    break;
            }
        }

    }

    public void activateModifyRawMaterialDialog() {
        JPanel modifyPanel = addCustomPanels.addBorderedPanel(Color.lightGray, BoxLayout.Y_AXIS, Color.RED, selectedRawMaterialModifyList.get(0).getName(), "Arial", Font.BOLD, 16, Color.RED, TitledBorder.CENTER, TitledBorder.TOP);
        modifyPrice = new AddCustomFields().addField(5, modifyPanel, true, String.valueOf(selectedRawMaterialModifyList.get(0).getPrice()), true);
        int nn = showMessage.showOption(null, "", connectionOptionNames, modifyPanel);
        if (nn == 0) {
            if (new RawMaterialRepository(userName, password).updatePrice(Float.parseFloat(modifyPrice.getText()), selectedRawMaterialModifyList.get(0).getRawMaterialId()) != 0) {
                showMessage.showMessage(" Updating price for " + selectedRawMaterialModifyList.get(0).getName(), 0);
                createTable();
                validate();
            } else
                showMessage.showMessage(" Error when updating price for " + selectedRawMaterialModifyList.get(0).getName(), 2);
        } else {
            showMessage.showMessage(" Operation aborted ", 0);
        }
        jTable.clearSelection();
    }

    public void activateModifyProductDialog() {
        JPanel modifyPanel = addCustomPanels.addBorderedPanel(Color.lightGray, BoxLayout.Y_AXIS, Color.RED, selectedProductModifyList.get(0).getName(), "Arial", Font.BOLD, 16, Color.RED, TitledBorder.CENTER, TitledBorder.TOP);
        modifyPrice = new AddCustomFields().addField(5, modifyPanel, true, String.valueOf(selectedProductModifyList.get(0).getPrice()), true);
        int nn = showMessage.showOption(null, "", connectionOptionNames, modifyPanel);
        if (nn == 0) {
            if (new ProductRepository(userName, password).updatePrice(Float.parseFloat(modifyPrice.getText()), selectedProductModifyList.get(0).getProductId()) != 0) {
                showMessage.showMessage(" Updating price for " + selectedProductModifyList.get(0).getName(), 0);
                createTable();
                validate();
            } else
                showMessage.showMessage(" Error when updating price for " + selectedProductModifyList.get(0).getName(), 2);
        } else {
            showMessage.showMessage(" Operation aborted ", 0);
        }
        jTable.clearSelection();
    }

    public void validate() {
        selectedProductPanel.removeAll();
        selectedProductPanel.repaint();
        selectedProductPanel.setLayout(new BorderLayout());
        tableScrollPane = new JScrollPane(jTable);
        tableScrollPane.setBackground(Color.white);
        tableScrollPane.addMouseListener(this);
        selectedProductPanel.add(tableScrollPane, BorderLayout.CENTER);
        selectedProductPanel.validate();
        frame.validate();
    }

    public void tableHeader(String[] header, boolean isEditable) {
        selectedProductPanel.removeAll();
        selectedProductPanel.repaint();
        jTable = new CreateTable().makeTable("", header, isEditable);
        model = (DefaultTableModel) jTable.getModel();
        tableModel = jTable.getModel();
        for (int i = 0; i < jTable.getColumnCount(); i++) {
            TableColumn col = jTable.getColumnModel().getColumn(i);
            jTable.getColumnModel().getColumn(i).setPreferredWidth(15);
            DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
            dtcr.setHorizontalAlignment(SwingConstants.CENTER);
            col.setCellRenderer(dtcr);
        }
        rowSelectionModel = jTable.getSelectionModel();
        jTable.addMouseListener(this);
    }

    public void searchName(String name) {
        switch (tableName) {
            case "Inputs":
                tableHeader(inputsHeaderLabel, false);
                rawInputsList = new RawInputsRepository(userName, password).searchInputs(name);
                if (rawInputsList.size() > 0) {
                    for (RawInputs rawInput : rawInputsList) {
                        Object[] row = new Object[]{rawInput.getDate(), rawInput.getProduct(), rawInput.getQuantity(), rawInput.getPrice()};
                        model.addRow(row);
                    }
                } else showMessage.showMessage("The information does not exist", 1);
                break;

            case "Category":
                tableHeader(categoryHeaderLabel, false);
                productCategoriesList = new ProductCategoryRepository(userName, password).searchByName(name);
                if (productCategoriesList.size() > 0) {
                    for (ProductCategory productCategory : productCategoriesList) {
                        Object[] row = new Object[]{productCategory.getName()};
                        model.addRow(row);
                    }
                } else showMessage.showMessage("The information does not exist", 1);
                break;

            case "Raw materials":
                tableHeader(rawMaterialsHeaderLabel, false);
                rawMaterialsList = new RawMaterialRepository(userName, password).searchByName(name);
                if (rawMaterialsList.size() > 0) {
                    for (RawMaterial rawMaterial : rawMaterialsList) {
                        Object[] row = new Object[]{rawMaterial.getName(), rawMaterial.getQuantity(), rawMaterial.getPrice()};
                        model.addRow(row);
                    }
                } else showMessage.showMessage("The information does not exist", 1);
                break;

            case "Products":
                tableHeader(productHeaderLabel, false);
                Product product = new ProductRepository(userName, password).searchByName(name);
                Object[] row = new Object[]{product.getName(), product.getPrice(), product.isState()};
                model.addRow(row);
                break;
        }
    }

    public void createTable() {
        switch (tableName) {
            case "Inputs":
                tableHeader(inputsHeaderLabel, false);
                rawInputsList = new RawInputsRepository(userName, password).findAllRawInputs();
                for (RawInputs rawInput : rawInputsList) {
                    Object[] row = new Object[]{rawInput.getDate(), rawInput.getProduct(), rawInput.getQuantity(), rawInput.getPrice()};
                    model.addRow(row);
                }
                break;

            case "Category":
                tableHeader(categoryHeaderLabel, false);
                productCategoriesList = new ProductCategoryRepository(userName, password).findAllProductCategories();
                for (ProductCategory productCategory : productCategoriesList) {
                    Object[] row = new Object[]{productCategory.getName()};
                    model.addRow(row);
                }
                break;

            case "Raw materials":
                tableHeader(rawMaterialsHeaderLabel, false);
                rawMaterialsList = new RawMaterialRepository(userName, password).findAllRawMaterials();
                for (RawMaterial rawMaterial : rawMaterialsList) {
                    Object[] row = new Object[]{rawMaterial.getName(), rawMaterial.getQuantity(), rawMaterial.getPrice()};
                    model.addRow(row);
                }
                break;

            case "Products":
                tableHeader(productHeaderLabel, false);
                productsList = new ProductRepository(userName, password).findAllProducts();
                for (Product product : productsList) {
                    Object[] row = new Object[]{product.getName(), product.getPrice(), product.isState()};
                    model.addRow(row);
                }
                break;

            case "Recipes":
                tableHeader(recipeHeaderLabel, false);
                List<Object[]> recipesWithMaterial = new RecipeRepository(userName, password).findAllRecipes();
                for (Object[] objects : recipesWithMaterial) {
                    model.addRow(objects);
                }
                break;

            case "Orders":
                tableHeader(orderHeaderLabel, false);
                // List<Object[]> ordersWithProducts = new MyRecipeRepository(userName, password).listWithProductAndMaterial("SELECT o.dateAndTime, p.name o.price, FROM MyRecipe m INNER JOIN Product p ON p.productId = m.productId INNER JOIN RawMaterial r ON m.rawMaterialId = r.rawMaterialId");

                ordersList = new OrderRepository(userName, password).findAllOrders();
                for (Order o : ordersList) {
                    Object[] row = new Object[]{longFormat(o.getDateAndTime()), o.getPrice(), ""};
                    model.addRow(row);
                }
                break;

            case "Daily":
                tableHeader(orderHeaderLabel, false);
                ordersList = new ReportsRepository(userName, password).getCount("FROM Order WHERE DATE(dateAndTime) = '" + selectedDate + "'");
                dailyAndMonthlyTable();
                break;

            case "Monthly":
                tableHeader(orderHeaderLabel, false);
                String[] yearAndMonth = selectedDate.split("-");
                ordersList = new ReportsRepository(userName, password).getCount("FROM Order WHERE YEAR(dateAndTime) = '" + yearAndMonth[0] + "' AND MONTH(dateAndTime) = '" + yearAndMonth[1] + "'");
                dailyAndMonthlyTable();
                break;

            case "Hourly":
                tableHeader(orderHeaderLabel, false);
                ordersList = new ReportsRepository(userName, password).getCount("FROM Order WHERE dateAndTime < '" + dateTime + "' AND dateAndTime > '" + dateTime.minusHours(1) + "'");
                dailyAndMonthlyTable();
                break;

            case "Minimal stock":
            case "Critical stock":
                rawMaterialsList = new RawMaterialRepository(userName, password).getCount(stock);
                if (rawMaterialsList.size() > 0) {
                    tableHeader(rawMaterialsHeaderLabel, false);
                    for (RawMaterial rawMaterial : rawMaterialsList) {
                        Object[] row = new Object[]{rawMaterial.getName(), rawMaterial.getQuantity(), rawMaterial.getPrice()};
                        model.addRow(row);
                    }
                    validate();
                } else showMessage.showMessage("No information available", 1);
                break;
        }
    }

    public void dailyAndMonthlyTable() {
        if (ordersList.size() > 0) {
            String records = " Found " + ordersList.size();
            if (ordersList.size() == 1) {
                records += " " + "record";
            } else {
                records += " " + "records";
            }
            showMessage.showMessage(records, 1);
            for (Order o : ordersList) {
                Object[] row = new Object[]{longFormat(o.getDateAndTime()), o.getPrice(), ""};
                model.addRow(row);
            }
            validate();
        } else showMessage.showMessage(" No information available ", 1);
    }

    public void mouseClicked(MouseEvent e) {
        try {
            switch (tableName) {
                case "Products":
                    selectedProductModifyList = (List<Product>) new ProductRepository(userName, password).searchByName((String) jTable.getValueAt(jTable.getSelectedRow(), 0));
                    break;
                case "Raw materials":
                    selectedRawMaterialModifyList = new RawMaterialRepository(userName, password).searchByName((String) jTable.getValueAt(jTable.getSelectedRow(), 0));
                    break;
            }
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {

        popup = new JPopupMenu();

        if (e.getComponent().equals(tableScrollPane) && model().isSelectionEmpty()) {
            setGeneralMenu();
        }
        if ((e.getModifiers() & InputEvent.BUTTON3_MASK) != 0) {
            if (e.getComponent().equals(jTable)) {
                if (model().isSelectionEmpty()) {
                    setGeneralMenu();
                } else if (!model().isSelectionEmpty()) {
                    startMenu();
                    switch (tableName) {
                        case "Raw materials":
                            modifyMenu();
                            break;
                        case "Category":
                            deleteMenu();
                            break;
                        case "Products":
                            modifyMenu();
                            deleteMenu();
                            break;
                    }
                }
                maybeShowPopup(e);
            }
        }
    }

    public void startMenu() {
        record = addMenu(" Records ", popup);
        addMenuItem(record, " Refresh ");
        switch (tableName) {
            case "Products":
            case "Category":
            case "Inputs":
                addMenuItem(record, " Add record ");
                break;
        }
    }

    public void addMenuItem(JMenu parent, String name) {
        menuItem = new JMenuItem(name);
        menuItem.addActionListener(this);
        parent.add(menuItem);
    }

    public void modifyMenu() {
        addMenuItem(record, " Modify record ");
    }

    public void deleteMenu() {
        addMenuItem(record, " Delete record ");
    }

    public void setGeneralMenu() {
        startMenu();
        JMenu search = addMenu(" Search ", popup);
        addMenuItem(search, " General search ");
    }

    public JMenu addMenu(String name, JPopupMenu popup) {
        JMenu menu = new JMenu(name);
        menu.setFont(new Font("Arial", Font.BOLD, 20));
        popup.add(menu);
        return menu;
    }

    public void mouseReleased(MouseEvent e) {
        maybeShowPopup(e);
    }

    public void maybeShowPopup(MouseEvent e) {
        if ((e.getModifiers() & InputEvent.BUTTON3_MASK) != 0) {
            if (e.isPopupTrigger()) popup.show(e.getComponent(), e.getX(), e.getY());
        }
    }

    public ListSelectionModel model() {
        return rowSelectionModel = jTable.getSelectionModel();
    }
}
