import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "raw_materials")
public class RawMaterial {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rawMaterialId")
    private int rawMaterialId;

    @Column(name = "name")
    private String name;

    @Column(name = "quantity")
    private float quantity;

    @OneToMany(cascade = CascadeType.ALL, mappedBy="rawMaterialId")
    private List<Recipe> recipes=new ArrayList<>();

    @Column(name="price")
    private float price;

    public RawMaterial() {
    }

    public RawMaterial(int rawMaterialId, String name, float quantity, float price) {
        this.rawMaterialId = rawMaterialId;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    public int getRawMaterialId() {
        return rawMaterialId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getQuantity() {
        return quantity;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
