import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class FormatDateTime {
    public String longFormat(LocalDateTime localDateTime) {
        return localDateTime.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)) + "  " + localDateTime.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
    }
}
