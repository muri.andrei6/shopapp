import javax.swing.*;

public class AddCustomFields {

    public JTextField addField(int dimension, JPanel panel, boolean state, String text, boolean noBorder) {
        JTextField textField = new JTextField(dimension);
        textField.setEditable(state);
        textField.setText(text);
        if (!noBorder) {
            textField.setHorizontalAlignment(JTextField.CENTER);
            textField.setBorder(javax.swing.BorderFactory.createEmptyBorder());
        }
        panel.add(textField);
        return textField;
    }

    public JPasswordField addPasswordField(JPanel p) {
        JPasswordField field = new JPasswordField();
        p.add(field);
        return field;
    }
}