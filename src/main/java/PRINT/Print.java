package PRINT;
import com.spire.pdf.PdfDocument;

import java.awt.*;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;

public class Print {

    public static void main(String[] args) throws IOException {

        File file = new File("C:\\Users\\Adrian\\Desktop\\");
        PdfDocument pdf = new PdfDocument();
        if (file.toString().endsWith("Receipt.pdf"))
            Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + file);
        else {
            Desktop desktop = Desktop.getDesktop();
            desktop.open(file);

            PrinterJob loPrinterJob = PrinterJob.getPrinterJob();
            PageFormat loPageFormat = loPrinterJob.defaultPage();

            //set the print page size
            Paper loPaper = loPageFormat.getPaper();
            loPaper.setSize(500, 600);
            loPageFormat.setPaper(loPaper);
            loPrinterJob.setPrintable(pdf, loPageFormat);

            try {
                loPrinterJob.print();
            } catch (PrinterException e) {
                e.printStackTrace();
            }
        }
    }
}