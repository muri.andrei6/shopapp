class AdminLogin {

    ShowMessage showMessage = new ShowMessage();
    private final static Object[] connectionOptionNames = {" Connect ", " Cancel "};

    public AdminLogin() {
        new LookAndFeel();

        activateConnectionDialog();
    }

    public void activateConnectionDialog() {
        int selectedOption = showMessage.showOption("", " Administrator ", connectionOptionNames, LoginComponent.userLoginPanel());
        if (selectedOption == 0) {
           if(Utils.getSessionFactory(LoginComponent.getUser(), LoginComponent.getPass()) != null) {
               new Admin(LoginComponent.getUser(), LoginComponent.getPass());
           }
           else error();
        }

        if (selectedOption != 0) {
            showMessage.showMessage("Connection aborted!", 1);
            System.exit(0);
        }
    }

    public void error() {
        showMessage.showMessage("You cannot connect to database.<br>Please check the user name or passoword.", 2);
        LoginComponent.setText("");
        activateConnectionDialog();
    }
}

