import org.hibernate.query.Query;

import java.util.List;

public class ProductRepository extends SessionTransaction {
    String userName, password;

    ProductRepository(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public void saveProduct(Product product) {
        open();
        session.save(product);
        closeSession();
    }

    public void deleteProduct(Product product) {
        open();
        session.delete(product);
        closeSession();
    }

    public Product findByProductId(int id) {
        open();
        Product product = session.find(Product.class, id);
        closeSession();
        return product;
    }

    public List findAllProducts() {
        open();
        String allProducts = ("FROM Product");
        Query query = session.createQuery(allProducts);
        List productList = query.list();
        closeSession();
        return productList;
    }

    public void updateObject(Object object) {
        openSession();
        session.update(object);
        closeSession();
    }

    public int updatePrice(float price, int productId) {
        openSession();
        Query query = session.createQuery("UPDATE Product SET price = '" + price +"' WHERE productId = " + productId);
        int result = query.executeUpdate();
        closeSession();
        return result;
    }

    public List<Product> findByCategoryId(int categoryId) {
        open();
        String allProducts = ("FROM Product WHERE productCategoryId = " + categoryId);
        Query query = session.createQuery(allProducts);
        List<Product> productList = query.list();
        closeSession();
        return productList;
    }

    public Product searchByName(String name) {
      //  public List searchByName(String name) {
        open();
        String searchByName = ("FROM Product WHERE name = '" + name + "'");
        Query query = session.createQuery(searchByName);
        List productList = query.list();
        closeSession();
        return (Product) productList.get(0);
    }

    public void open() {
        setUserAndPassword(userName, password);
        openSession();
    }
}
