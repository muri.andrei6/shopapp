import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class AppCalendar extends JFrame implements ActionListener {

    Planner planner = new Planner();

    LocalTime currentHour;

    String currentData;
    String buttonBack = "C:\\NutriPlan\\sources\\images\\StepBack24.gif";
    String buttonNext = "C:\\NutriPlan\\sources\\images\\StepForward24.gif";

    static String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    String[] weekdays = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};

    JButton prevMonthButton, nextMonthButton;

    JPanel calendarPanel;

    static JLabel monthLabel, emptyLabel, yearLabel;

    static int selectedDay = -1;
    static int month;
    static int year;
    private boolean view;
    private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    String[] connectionOptionNames = {" Save ", " Cancel "};

    JPanel panel;

    public AppCalendar() {
       JPanel top = new AddCustomPanels().addSimplePanel(Color.green, null, new GridLayout(), 0, 0);
        prevMonthButton = addMyButton(" Prev ", new ImageIcon(buttonBack), top, Color.blue, Color.green, 20, 0, true, 50, 20);
        monthLabel = AddCustomLabels.addLabel("   ", top, "Arial", Font.BOLD, 20, Color.black, Color.green, SwingConstants.CENTER);
        yearLabel = AddCustomLabels.addLabel("   ", top, "Arial", Font.BOLD, 20, Color.black, Color.green, SwingConstants.CENTER);
        nextMonthButton = addMyButton(" Next ", new ImageIcon(buttonNext), top, Color.blue, Color.green, 20, 1, true, 50, 20);

        JPanel bottom = new AddCustomPanels().addSimplePanel(Color.green, null, new GridLayout(1, weekdays.length), 0, 0);
        for (String weekday : weekdays) {
            new AddCustomLabels().addLabel(weekday, bottom, "Arial", Font.BOLD, 16, Color.black, Color.green, SwingConstants.CENTER, null, JLabel.CENTER);
        }

        calendarPanel = new AddCustomPanels().addSimplePanel(Color.green, new BorderLayout(), null, 0, 0);
        calendarPanel.setPreferredSize(new Dimension(460, 200));

        JPanel dataPanel = new AddCustomPanels().addSimplePanel(Color.green, null, new GridLayout(), 0, 0);
        emptyLabel = AddCustomLabels.addLabel("   ", dataPanel, "Arial", Font.BOLD, 20, Color.white, new Color(128, 0, 128), SwingConstants.CENTER);

        JPanel masterDatePanel = new AddCustomPanels().addSimplePanel(Color.green, null, null, 0, BoxLayout.Y_AXIS);
        masterDatePanel.add(top);
        masterDatePanel.add(dataPanel);
        masterDatePanel.add(bottom);
        masterDatePanel.add(calendarPanel);
        setDataAndTime();
        updateCalendarDisplay(month, year);

        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        panel.add(masterDatePanel);

    }

    public void setView(boolean view){
        this.view = view;
        if (view) {
            panel.add(planner.getMasterPanel());
        }
    }

    public String activateViewCalendar() {
        int nn = JOptionPane.showOptionDialog(this, panel, " Data ", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, connectionOptionNames, null);
        if (nn == 0) return getDateString() + " " + planner.getSelectedHour();
        else return null;
    }

    public JButton addMyButton(String name, ImageIcon image, JPanel panel, Color foreground, Color background, int iconGap, int iconLeftSide, boolean state, int width, int height) {
        JButton button = new AddCustomButtons().addButton(name, image, panel, foreground, background, iconGap, iconLeftSide, state, width, height);
        button.addActionListener(this);
        return button;
    }

    public void setDataAndTime() {
        GregorianCalendar today = new GregorianCalendar();
        month = today.get(Calendar.MONTH);
        year = today.get(Calendar.YEAR);
        currentData = months[month] + " " + today.get(Calendar.DAY_OF_MONTH) + ", " + today.get(Calendar.YEAR);
        currentHour = LocalTime.of(today.get(Calendar.HOUR_OF_DAY), 0, 0);
        emptyLabel.setText(currentData);

        if (view) Planner.setCurrentData(LocalDate.now());
    }

    public void actionPerformed(ActionEvent e) {
        try {
            boolean state = true;
            int cc = Integer.parseInt(e.getActionCommand());
            if (cc > 0) {
                GregorianCalendar today = new GregorianCalendar();
                selectedDay = cc;
                if (year > today.get(Calendar.YEAR)) state = false;
                if (year >= today.get(Calendar.YEAR) && month > today.get(Calendar.MONTH)) state = false;
                if (month >= today.get(Calendar.MONTH) && cc > today.get(Calendar.DAY_OF_MONTH)) state = false;
                if (year < today.get(Calendar.YEAR) && month >= today.get(Calendar.MONTH) && cc > today.get(Calendar.DAY_OF_MONTH))
                    state = true;

                if (!state) new ShowMessage().showMessage("Please select date less than current date", 1);
                else {
                    emptyLabel.setText(getSelectionString());
                    if (view) {
                        Planner.updateAppointmentsPanel(getDateString());
                        planner.setDateString(getSelectionString());
                    }
                }
            }
        } catch (NumberFormatException ignored) {
        }

        if (e.getActionCommand().equals(" Prev ")) {
            month = month - 1;
            if (month < 0) {
                month = 11;
                year = year - 1;
            }
            emptyLabel.setText(" ");
        }

        if (e.getActionCommand().equals(" Next ")) {
            month = month + 1;
            if (month == 12) {
                month = 0;
                year = year + 1;
            }
            emptyLabel.setText(" ");
        }
        updateCalendarDisplay(month, year);
    }

    public void updateCalendarDisplay(int month, int year) {
        monthLabel.setText(months[month]);
        yearLabel.setText(String.valueOf(year));

        calendarPanel.removeAll();
        calendarPanel.repaint();

        GregorianCalendar today = new GregorianCalendar();
        GregorianCalendar firstDayOfMonth = new GregorianCalendar(year, month, 1);
        GregorianCalendar lastDayOfMonth = new GregorianCalendar(year, month, firstDayOfMonth.getActualMaximum(Calendar.DAY_OF_MONTH));

        int weeksInMonth = lastDayOfMonth.get(Calendar.WEEK_OF_MONTH);
        calendarPanel.setLayout(new GridLayout(weeksInMonth, 7, 2, 2));
        int firstDayInFirstWeek = firstDayOfMonth.get(Calendar.DAY_OF_WEEK);
        for (int i = 1; i < firstDayInFirstWeek; i++) {
            JButton blank = addMyButton("", null, calendarPanel, Color.green, Color.green, 0, 1, true, 5, 5); //dark turquoise
            blank.setEnabled(false);
        }
        GregorianCalendar currentDay;
        for (currentDay = firstDayOfMonth; !currentDay.equals(lastDayOfMonth); currentDay.roll(Calendar.DAY_OF_MONTH, 1)) {
            JButton nextButton = addMyButton(String.valueOf(currentDay.get(Calendar.DAY_OF_MONTH)), null, calendarPanel, Color.blue, Color.lightGray, 0, 1, true, 5, 5); //dark turquoise
            if (isOnWeekend(currentDay)) nextButton.setForeground(Color.red);
            if (compare(currentDay, today)) {
                nextButton.setForeground(Color.red);
                nextButton.setBackground(Color.black);
            }
        }

        JButton lastDayButton = addMyButton(String.valueOf(lastDayOfMonth.get(Calendar.DAY_OF_MONTH)), null, calendarPanel, Color.blue, Color.lightGray, 0, 1, true, 5, 5); //dark turquoise
        if (isOnWeekend(lastDayOfMonth)) lastDayButton.setForeground(Color.red);
        if (compare(lastDayOfMonth, today)) {
            lastDayButton.setForeground(Color.red);
            lastDayButton.setBackground(Color.yellow);
        }
        int lastDayInLastWeek = lastDayOfMonth.get(Calendar.DAY_OF_WEEK);
        for (int i = lastDayInLastWeek + 1; i <= 7; i++) {
            JButton blank = addMyButton("", null, calendarPanel, Color.green, Color.green, 0, 1, true, 5, 5); //dark turquoise
            blank.setEnabled(false);
        }
        calendarPanel.validate();
    }

    public LocalDate getDateString() {
        return LocalDate.parse(getYearSelection() + "-" + getIntMonthSelection() + "-" + getDayOfMonthSelection(), dtf);
    }

    public String getIntMonthSelection() {
        if (month + 1 < 10) return ("0" + (month + 1));
        else return String.valueOf(month + 1);
    }

    public static String getSelectionString() {
        return getMonthSelection() + " " + getDayOfMonthSelection() + ", " + getYearSelection();
    }

    public static String getMonthSelection() {
        return months[month];
    }

    public static String getDayOfMonthSelection() {
        if (selectedDay < 10) return "0" + selectedDay;
        else return String.valueOf(selectedDay);
    }

    public static String getYearSelection() {
        return String.valueOf(year);
    }

    public boolean isOnWeekend(Calendar calendar) {
        return ((calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) ||
                (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY));
    }

    private boolean compare(GregorianCalendar first, GregorianCalendar second) {
        return ((first.get(Calendar.DAY_OF_MONTH) == second.get(Calendar.DAY_OF_MONTH))
                && (first.get(Calendar.MONTH) == second.get(Calendar.MONTH))
                && (first.get(Calendar.YEAR) == second.get(Calendar.YEAR)));
    }
}
