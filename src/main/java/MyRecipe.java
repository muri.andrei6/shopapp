import javax.persistence.*;

@Entity
@Table(name = "recipe_1")
public class MyRecipe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "productId")
    private int productId;

    @Column(name = "rawMaterialId")
    private int rawMaterialId;

    @Column(name = "quantity")
    private float quantity;

    public MyRecipe(int id, int productId, int rawMaterialId, float quantity) {
        this.id = id;
        this.productId = productId;
        this.rawMaterialId = rawMaterialId;
        this.quantity = quantity;
    }

    public MyRecipe() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getRawMaterialId() {
        return rawMaterialId;
    }

    public void setRawMaterialId(int rawMaterialId) {
        this.rawMaterialId = rawMaterialId;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Recipe_1{" +
                "id=" + id +
                ", productId=" + productId +
                ", rawMaterialId=" + rawMaterialId +
                ", quantity=" + quantity +
                '}';
    }
}
