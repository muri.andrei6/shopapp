import BARCODE.Barcode;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.List;

public class Shop_1 implements ActionListener, ListSelectionListener {

    JPanel productPanel, selectedProductPanel;

    JButton button, completeButton, deleteButton, decreaseButton;

    String[] headerLabel = {"Product", "Price", "Quantity", "Final price"};

    JFrame frame;
    JTable jTable;
    DefaultTableModel model;
    JTextField totalAmountField;
    JPanel southProductPanel, categoryPanel, totalAmountPanel;
    Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

    private final static Object[] confirmationOptionNames = {" Yes ", " No "};
    boolean initCounter = true, counterRowState = false;
    DecimalFormat df = new DecimalFormat("#.##");

    int counterRow;
    ListSelectionModel selectionModel;
    String userName, password;

    List<ProductCategory> categoryList;
    List<Product> productList;
    List<Product> orderProductList;

    ProductRepository productRepository;
    OrderRepository orderRepository;
    ProductCategoryRepository productCategoryRepository;
    RawMaterialRepository rawMaterialRepository;
    ShowMessage showMessage = new ShowMessage();

    public Shop_1(String userName, String password) {
        this.userName = userName;
        this.password = password;

        productRepository = new ProductRepository(userName, password);
        orderRepository = new OrderRepository(userName, password);
        productCategoryRepository = new ProductCategoryRepository(userName, password);
        rawMaterialRepository = new RawMaterialRepository(userName, password);

        jTable = new CreateTable().makeTable("", headerLabel, false);
        model = (DefaultTableModel) jTable.getModel();

        jTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        selectionModel = jTable.getSelectionModel();

        selectionModel.addListSelectionListener(this);

        categoryList = productCategoryRepository.findAllProductCategories();
        categoryPanel = new AddCustomPanels().addSimplePanel(new Color(64, 224, 208), null, new GridLayout((int) Math.round(categoryList.size() / 2.0), 2), 5, 0);
        for (ProductCategory c : categoryList) {
            addMyButton(c.getName(), categoryPanel, Color.black, new Color(64, 224, 208), true);
        }

        productPanel = new AddCustomPanels().addSimplePanel(new Color(128, 0, 128), null, new GridLayout(), 5, 0);

        selectedProductPanel = new AddCustomPanels().addSimplePanel(Color.white, null, new GridLayout(), 5, 0);

        totalAmountPanel = new AddCustomPanels().addSimplePanel(Color.white, null, new GridLayout(1, 2), 0, BoxLayout.X_AXIS);
        new AddCustomLabels().addLabel("Total de plata", totalAmountPanel, "Arial", Font.BOLD, 24, Color.black, Color.white, SwingConstants.CENTER, BorderLayout.NORTH, JLabel.CENTER);
        totalAmountField = new AddCustomFields().addField(24, totalAmountPanel, false, "0", false);

        JPanel deletePanel = new AddCustomPanels().addSimplePanel(Color.white, null, new GridLayout(1, 2), 0, BoxLayout.X_AXIS);
        decreaseButton = new AddCustomButtons().addSimpleButton(" Decrease quantity ", deletePanel, Color.yellow, Color.red, false, 100, 80);
        decreaseButton.addActionListener(this);

        deleteButton = new AddCustomButtons().addSimpleButton(" Delete product ", deletePanel, Color.yellow, Color.red, false, 100, 80);
        deleteButton.addActionListener(this);

        JPanel buttonAmountPanel = new AddCustomPanels().addSimplePanel(Color.white, null, new GridLayout(1, 2), 0, BoxLayout.X_AXIS);
        completeButton = addMyButton("Complete the order", buttonAmountPanel, Color.BLACK, Color.green, false);
        addMyButton("Cancel order", buttonAmountPanel, Color.BLACK, Color.orange, true);
        southProductPanel = new AddCustomPanels().addSimplePanel(Color.white, null, null, 0, BoxLayout.Y_AXIS);
        southProductPanel.add(deletePanel);
        southProductPanel.add(totalAmountPanel);
        southProductPanel.add(buttonAmountPanel);

        categoryPanel.setPreferredSize(new Dimension(250, dim.height - 10));
        categoryPanel.setMaximumSize(new Dimension(250, dim.height));
        productPanel.setPreferredSize(new Dimension(240, dim.height - 10));
        productPanel.setMaximumSize(new Dimension(240, dim.height));
        selectedProductPanel.setPreferredSize(new Dimension(dim.width - 300, dim.height));
        selectedProductPanel.setMaximumSize(new Dimension(dim.width - 300, dim.height));

        frame = new JFrame();
        frame.setSize(dim.width, dim.height);
        frame.setBackground(Color.WHITE);
        frame.getContentPane().setLayout(new BorderLayout());
        frame.add(categoryPanel, BorderLayout.WEST);
        frame.add(selectedProductPanel, BorderLayout.CENTER);
        frame.add(productPanel, BorderLayout.EAST);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle(" My Shop ");

        frame.setResizable(false);
        frame.setVisible(true);
    }

    public JButton addMyButton(String name, JPanel panel, Color textColor, Color backgroundColor, boolean state) {
        button = new AddCustomButtons().addButton(name, null, panel, textColor, backgroundColor, 0, 1, state, 100, 80);
        button.addActionListener(this);
        return button;
    }

    public void setProductsPanel(List<Product> products) {
        if (initCounter) {
            setCenterPanel();
        }
        productPanel.removeAll();
        productPanel.repaint();

        productPanel.setLayout(new GridLayout((int) Math.round(products.size() / 2.0), 2));
        for (Product p : products) {
            addMyButton(p.getName(), productPanel, Color.yellow, new Color(128, 0, 128), p.isState());
        }
        productPanel.validate();
        frame.validate();
        initCounter = false;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        for (ProductCategory productCategory : categoryList) {
            if (e.getActionCommand().equals(productCategory.getName())) {
                productList = productRepository.findByCategoryId(productCategory.getProductCategoryId());
                if (productList.size() > 0) setProductsPanel(productList);
                else
                    new ShowMessage().showMessage("<html><p style=\"text-align:center;\">Category unavailable</p></html>", 1);
            }
        }
        switch (e.getActionCommand()) {
            case "Cancel order":
                reInit();
                break;

            case "Complete the order":
                if (showMessage.showOption("You have to pay " + totalAmountField.getText(), "", confirmationOptionNames, null) == 0) {
                    new ShowMessage().showMessage("<html><p style=\"text-align:center;\">Your order will be processed<br>Please press ok button for the receipt</p></html>", 0);
                    for (int i = 0; i < jTable.getRowCount(); i++) {
                        Product product = productRepository.searchByName((String) jTable.getValueAt(i, 0));
                        List<Recipe> orderRecipe = new RecipeRepository(userName, password).findByProductId(product.getProductId());
                        for (Recipe recipe : orderRecipe) {
                            float qq = rawMaterialRepository.findById(recipe.getRawMaterial().getRawMaterialId()).getQuantity() - recipe.getQuantity() * (int) jTable.getValueAt(i, 2);
                            new RawMaterialRepository(userName, password).update("UPDATE RawMaterial SET quantity = " + qq + " WHERE rawMaterialId = '" + recipe.getRawMaterial().getRawMaterialId() + "'");
                        }
                    }
                    orderRepository.saveOrder(new Order(LocalDateTime.now(), Float.parseFloat(totalAmountField.getText()), orderProductList));
                }
                reInit();
                 new Barcode().createPDF("Receipt.pdf", String.valueOf(orderRepository.getMaxId()));
              break;
            case " Decrease quantity ":
                int decreaseQuantity = (int) jTable.getValueAt(jTable.getSelectedRow(), 2);
                if (decreaseQuantity > 1) {
                    decreaseQuantity--;
                    jTable.setValueAt(decreaseQuantity, jTable.getSelectedRow(), 2);
                    float intPrice = (float) jTable.getValueAt(jTable.getSelectedRow(), 1) * decreaseQuantity;
                    jTable.setValueAt(Float.parseFloat(df.format(intPrice)), jTable.getSelectedRow(), 3);
                }
                if ((int) jTable.getValueAt(jTable.getSelectedRow(), 2) == 1) {
                    decreaseButton.setEnabled(false);
                }
                jTable.clearSelection();
                setTotalAmount();
                break;
            case " Delete product ":
                if (jTable.getRowCount() > 0) {
                    model.removeRow(jTable.getSelectedRow());
                    jTable.setModel(model);
                    decreaseButton.setEnabled(false);
                    setTotalAmount();
                }
                if (jTable.getRowCount() == 0) {
                    deleteButton.setEnabled(false);
                    completeButton.setEnabled(false);
                    decreaseButton.setEnabled(false);
                }
                break;
            default:
                for (int i = 0; i < model.getRowCount(); i++) {
                    if (jTable.getValueAt(i, 0).equals(e.getActionCommand())) {
                        counterRow = i;
                        counterRowState = true;
                        break;
                    }
                }
                if (counterRowState) {
                    int quantity = (int) jTable.getValueAt(counterRow, 2);
                    quantity++;
                    jTable.setValueAt(quantity, counterRow, 2);
                    float intPrice = (float) jTable.getValueAt(counterRow, 1) * quantity;
                    jTable.setValueAt(Float.parseFloat(df.format(intPrice)), counterRow, 3);
                } else {
                    for (Product value : productList) {
                        if (e.getActionCommand().equals(value.getName())) {
                            Object[] row = {value.getName(), value.getPrice(), 1, value.getPrice()};
                            model.addRow(row);
                        }
                    }
                }
                setTotalAmount();
                completeButton.setEnabled(true);
                counterRow = 0;
                counterRowState = false;
                break;
        }
    }

    public void setTotalAmount() {
        float total = 0;
        for (int i = 0; i < model.getRowCount(); i++) {
            total += (float) jTable.getValueAt(i, 3);
        }
        totalAmountField.setText(df.format(total));
    }

    public void reInit() {
        model.getDataVector().removeAllElements();
        model.fireTableDataChanged();
        initCounter = true;
        totalAmountField.setText("0");
        selectedProductPanel.removeAll();
        productPanel.removeAll();
        selectedProductPanel.repaint();
        productPanel.repaint();
        selectedProductPanel.validate();
        productPanel.validate();
        frame.validate();
    }

    public void setCenterPanel() {
        selectedProductPanel.removeAll();
        selectedProductPanel.repaint();

        for (int i = 0; i < jTable.getColumnCount(); i++) {
            TableColumn col = jTable.getColumnModel().getColumn(i);
            jTable.getColumnModel().getColumn(i).setPreferredWidth(15);
            DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
            dtcr.setHorizontalAlignment(SwingConstants.CENTER);
            col.setCellRenderer(dtcr);
        }
        selectedProductPanel.setLayout(new BorderLayout());
        JScrollPane tableScrollPane = new JScrollPane(jTable);
        tableScrollPane.setBackground(Color.white);
        selectedProductPanel.add(tableScrollPane, BorderLayout.CENTER);
        selectedProductPanel.add(southProductPanel, BorderLayout.SOUTH);
        selectedProductPanel.validate();
        frame.validate();
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        int selectedRows = jTable.getSelectedRow();
        if (selectedRows >= 0) {
            deleteButton.setEnabled(true);
            if ((int) jTable.getValueAt(selectedRows, 2) > 1)
                decreaseButton.setEnabled(true);
        }
    }
}
