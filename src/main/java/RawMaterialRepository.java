import org.hibernate.query.Query;

import java.util.List;

public class RawMaterialRepository extends SessionTransaction {
    String userName, password;

    RawMaterialRepository(String userName, String password){
        this.userName = userName;
        this.password = password;
    }

    public void saveRawMaterial(RawMaterial rawMaterial) {
        open();
        session.save(rawMaterial);
        closeSession();
    }

    public void deleteRawMaterial(RawMaterial rawMaterial) {
        open();
        session.delete(rawMaterial);
        closeSession();
    }

    public RawMaterial findById(int id) {
        open();
        RawMaterial rawMaterial = session.find(RawMaterial.class, id);
        closeSession();
        return rawMaterial;
    }

    public List<RawMaterial> findAllRawMaterials() {
        open();
        String allRawMaterials = ("FROM RawMaterial");
        Query query = session.createQuery(allRawMaterials);
        List<RawMaterial> rawMaterialList = ((org.hibernate.query.Query) query).list();
        closeSession();
        return rawMaterialList;
    }

    public int update(String command) {
        open();
        Query query = session.createQuery(command);
        int result = query.executeUpdate();
        closeSession();
        return result;
    }

    public List<RawMaterial> searchByName(String name) {
        open();
        String searchByName = ("FROM RawMaterial WHERE name = '" + name + "'");
        Query query = session.createQuery(searchByName);
        List<RawMaterial> rawMaterialList = query.list();
        closeSession();
        return rawMaterialList;
    }

    public List<RawMaterial> getCount(String stock) {
        open();
        Query query = session.createQuery(stock);
        List<RawMaterial> rawMaterialList = query.list();
        closeSession();
        return rawMaterialList;
    }

    public int updatePrice(float price, int rawMaterialId) {
        openSession();
        Query query = session.createQuery("UPDATE RawMaterial SET price = '" + price +"' WHERE rawMaterialId = " + rawMaterialId);
        int result = query.executeUpdate();
        closeSession();
        return result;
    }
    public void open(){
        setUserAndPassword(userName, password);
        openSession();
    }
}
