import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

public class SaveRawMaterial implements ActionListener, ListSelectionListener, MouseListener {
    JPanel selectedProductPanel;
    JFrame frame;
    JPanel southProductPanel;

    String userName, password;

    String[] inputsHeaderLabel = {"Product", "Quantity", "Price"};
    JButton addButton, deleteButton, saveButton;
    JTable jTable;
    TableModel tableModel;
    DefaultTableModel model;

    JScrollPane tableScrollPane;
    ListSelectionModel rowSelectionModel;
    List<RawMaterial> rawMaterials;
    boolean successfully = false;

    SaveRawMaterial(String userName, String password) {
        this.userName = userName;
        this.password = password;

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

        selectedProductPanel = new AddCustomPanels().addSimplePanel(Color.white, null, new GridLayout(), 5, 0);

        JPanel deletePanel = new AddCustomPanels().addSimplePanel(Color.white, null, new GridLayout(1, 2), 0, BoxLayout.X_AXIS);
        addButton = new AddCustomButtons().addSimpleButton(" Add product ", deletePanel, Color.yellow, Color.red, true, 100, 80);
        addButton.addActionListener(this);

        deleteButton = new AddCustomButtons().addSimpleButton(" Delete product ", deletePanel, Color.yellow, Color.red, false, 100, 80);
        deleteButton.addActionListener(this);

        JPanel saveButtonPanel = new AddCustomPanels().addSimplePanel(Color.white, null, new GridLayout(1, 1), 0, BoxLayout.X_AXIS);
        saveButton = new AddCustomButtons().addSimpleButton(" Save products ", saveButtonPanel, Color.black, Color.green, false, 100, 80);
        saveButton.addActionListener(this);

        southProductPanel = new AddCustomPanels().addSimplePanel(Color.white, null, null, 0, BoxLayout.Y_AXIS);
        southProductPanel.add(deletePanel);
        southProductPanel.add(saveButtonPanel);
        mainTable();
        initTable();


        frame = new JFrame();
        frame.setSize(dim.width, dim.height);
        frame.setBackground(Color.WHITE);
        frame.getContentPane().setLayout(new BorderLayout());
        frame.add(selectedProductPanel);
         frame.setTitle(" Admin ");

        frame.setResizable(false);
        validate();
        frame.setVisible(true);
    }

    public void initTable() {
        jTable = new CreateTable().makeTable("", inputsHeaderLabel, true);
        model = (DefaultTableModel) jTable.getModel();
        tableModel = jTable.getModel();
        for (int i = 0; i < jTable.getColumnCount(); i++) {
            TableColumn col = jTable.getColumnModel().getColumn(i);
            jTable.getColumnModel().getColumn(i).setPreferredWidth(15);
            DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
            dtcr.setHorizontalAlignment(SwingConstants.CENTER);
            col.setCellRenderer(dtcr);
        }
        rowSelectionModel = jTable.getSelectionModel();
        rowSelectionModel.addListSelectionListener(this);
        jTable.addMouseListener(this);
    }

    public void mainTable() {
        selectedProductPanel.removeAll();
        selectedProductPanel.repaint();
    }

    public void validate() {
        selectedProductPanel.setLayout(new BorderLayout());
        tableScrollPane = new JScrollPane(jTable);
        tableScrollPane.setBackground(Color.white);
        tableScrollPane.addMouseListener(this);
        selectedProductPanel.add(tableScrollPane, BorderLayout.CENTER);
        selectedProductPanel.add(southProductPanel, BorderLayout.SOUTH);
        selectedProductPanel.validate();
        frame.validate();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(" Add product ")) {
            Object[] row = {"", "", ""};
            model.addRow(row);
            enableButtons();
        }

        if (e.getActionCommand().equals(" Save products ")) {
            for (int i = 0; i < jTable.getRowCount(); i++) {
                float price = Float.parseFloat((String) jTable.getValueAt(i, 2));
                float quantity = Float.parseFloat((String) jTable.getValueAt(i, 1));

                rawMaterials = new RawMaterialRepository(userName, password).searchByName((String) jTable.getValueAt(i, 0));
                if (rawMaterials.size() == 0) {
                    new RawMaterialRepository(userName, password).saveRawMaterial(new RawMaterial(1, (String) jTable.getValueAt(i, 0), quantity, price));
                    new RawInputsRepository(userName, password).saveRawInputs(new RawInputs(1, java.time.LocalDate.now(), price, (String) jTable.getValueAt(i, 0), (int) quantity));
                } else {
                    float finalQuantity = quantity + rawMaterials.get(0).getQuantity();
                    int rwaMaterialId = rawMaterials.get(0).getRawMaterialId();
                    if (new RawMaterialRepository(userName, password).update("UPDATE RawMaterial SET quantity = " + finalQuantity + " WHERE rawMaterialId = " + rwaMaterialId) == 1) {
                        new RawInputsRepository(userName, password).saveRawInputs(new RawInputs(1, java.time.LocalDate.now(), price, (String) jTable.getValueAt(i, 0), (int) quantity));
                    } else
                        new ShowMessage().showMessage("<html><p style=\"text-align:center;\"> Error raw materials </p></html>", 2);
                }
            }
            jTable.clearSelection();
            model.getDataVector().removeAllElements();
            model.fireTableDataChanged();
            saveButton.setEnabled(false);
            deleteButton.setEnabled(false);
            successfully = true;
            frame.setVisible(false);
        }
        if (e.getActionCommand().equals(" Delete product ")) {
            if (jTable.getRowCount() >= 0) {
                model.removeRow(jTable.getSelectedRow());
                jTable.setModel(model);
            }
            enableButtons();
        }
    }
    public void enableButtons() {
        saveButton.setEnabled(jTable.getRowCount() > 0);
        deleteButton.setEnabled(jTable.getRowCount() > 0);
    }
    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        int selectedRows = jTable.getSelectedRow();
        if (selectedRows >= 0) deleteButton.setEnabled(true);
    }

    public boolean successfully(){
        return successfully;
    }
}
