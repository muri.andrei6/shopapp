import javax.swing.*;
import java.awt.*;

public class ShowMessage extends JFrame {

    public void showMessage(String message, int code) {
        switch (code) {
            case 0:
                JOptionPane.showMessageDialog(new ShowMessage(), getMessagePanel("<font color=#0000FF>" + message), " Information ", JOptionPane.PLAIN_MESSAGE);
                break;
            case 1:
                JOptionPane.showMessageDialog(new ShowMessage(), getMessagePanel("<font color=#661144>" + message), " Attention ", JOptionPane.WARNING_MESSAGE);
                break;
            case 2:
                JOptionPane.showMessageDialog(new ShowMessage(), getMessagePanel("<font color=#FF1100>" + message), " Error ", JOptionPane.ERROR_MESSAGE);
                break;
            case 3:
                JOptionPane.showMessageDialog(new ShowMessage(), getMessagePanel(message), " Question ", JOptionPane.QUESTION_MESSAGE);
                break;
            default:
                JOptionPane.showMessageDialog(new ShowMessage(), getMessagePanel(message), "", JOptionPane.PLAIN_MESSAGE);
        }
    }

    public JPanel getMessagePanel(String msg) {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        new AddCustomLabels().addLabel("<html><p style=\"text-align:center;\">" + msg + "</p></html>", panel, "Arial", Font.BOLD, 16, Color.black, Color.lightGray, SwingConstants.CENTER, null, JLabel.CENTER);
        return panel;
    }

    public int showOption(String message, String title, Object[] options, JPanel optionPanel) {
        if (optionPanel == null)
            return JOptionPane.showOptionDialog(new ShowMessage(), getMessagePanel(message), title,
                    JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
        else
            return JOptionPane.showOptionDialog(new ShowMessage(), optionPanel, title, JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
    }

    public String showInputDialog() {
        return  (String) JOptionPane.showInputDialog(new ShowMessage(), "", " Search ", JOptionPane.QUESTION_MESSAGE, null, null, null);
    }
}
