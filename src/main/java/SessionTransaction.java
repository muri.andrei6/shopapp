import org.hibernate.Session;
import org.hibernate.Transaction;

public class SessionTransaction {
    Session session;
    Transaction transaction;
    String userName, password;

    public void setUserAndPassword(String userName, String password){
        this.userName = userName;
        this.password = password;
    }

    public void openSession() {
        session = Utils.getSessionFactory(userName, password).openSession();
        transaction = session.beginTransaction();
    }

    public void closeSession() {
        transaction.commit();
        session.close();
    }
}
