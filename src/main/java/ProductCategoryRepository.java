import org.hibernate.query.Query;

import java.util.List;

public class ProductCategoryRepository extends SessionTransaction {

    String userName, password;

    ProductCategoryRepository(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public void saveProductCategory(ProductCategory productCategory) {
        open();
        session.save(productCategory);
        closeSession();
    }

    public void deleteProductCategory(ProductCategory productCategory) {
        open();
        session.delete(productCategory);
        closeSession();
    }

    public ProductCategory findById(int id) {
        open();
        ProductCategory productCategory = session.find(ProductCategory.class, id);
        closeSession();
        return productCategory;
    }

    public List<ProductCategory> findAllProductCategories() {
        open();
        String allProductCategories = ("FROM ProductCategory");
        Query query = session.createQuery(allProductCategories);
        List<ProductCategory> productCategoryList = query.list();
        closeSession();
        return productCategoryList;
    }

    public List<ProductCategory> searchByName(String name) {
        open();
        String searchByName = ("FROM ProductCategory WHERE name = '" + name + "'");
        Query query = session.createQuery(searchByName);
        List<ProductCategory> productCategoryList = query.list();
        closeSession();
        return productCategoryList;
    }

    public void open() {
        setUserAndPassword(userName, password);
        openSession();
    }
}
